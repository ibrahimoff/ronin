<?php include './includes/header.inc'?>
	<!-- Team Detail -->
	<section id="team-detail">
		<!-- Section Header -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->teammate_position?></h3>
					<a href="<?php echo $_SERVER['HTTP_REFERER']?>" class="btn btn-default v-center"><?php echo __("BACK TO TEAM LIST");?></a>
				</div>
			</div>
		</div>
		<!-- Section Header End -->
		<!-- Section Content -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="row">
					<div class="col-md-3">
						<!-- Team Photo -->
						<a href="<?php echo $page->teammate_photo->url?>" data-rel="prettyPhoto" class="zoom-effect"><img src="<?php echo $page->teammate_photo->url?>" alt="<?php echo $page->title?>" class="img-full"></a>
						<!-- Team Photo End -->
						
						<!-- Team Social -->
				
    						<ul class="social-icons social-grey margin-top-20 nav-default clearfix">
    							<li><a href="<?php echo $page->social_facebook?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
    							<li><a href="<?php echo $page->social_twitter?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
    							<li><a href="<?php echo $page->social_google?>" class="google"><i class="fa fa-google-plus"></i></a></li>
    							<li><a href="<?php echo $page->social_linkedin?>" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
    						</ul>
		

						<!-- Team Social End -->
						
						<!-- Team Skills -->
						<h4 class="margin-top-20"><?php echo __("SKILLS");?></h4>
						<?php foreach ($page->skills as $skill):?>
    						<div class="progress">
    							<div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $skill->skill_value?>%">
    								<?php echo $skill->skill_name?>
    							</div>
    						</div>
						<?php endforeach;?>
						<!-- Team Skills End -->
					</div>
					<div class="col-md-9 justify">
						<!-- Team Description -->
						<h2 class="text-bold text-space line-bottom left"><?php echo $page->page_subheading?></h2>
                        <?php echo $page->body?>
						<!-- Team Description End -->
					</div>
				</div>
			</div>
		</div>
		<!-- Section Content End -->
		<!-- Team -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header">
					<h2><?php echo $page->headline?></h2>
					<h3><?php echo $page->after_headline_text?></h3>
				</div>
				<!-- Section Header End -->
				
				<!-- Section Content -->
				<div class="row">
					<div class="team-horizontal" id="owl-team">
						<?php foreach($pages->get("/about/")->children as $team):?>
							<div class="team">
								<div class="team-pic"><a href="<?php echo $team->url?>"><img src="<?php echo $team->teammate_photo->url?>" alt="<?php echo $team->title?>" class="img-full"></a></div>
								<div class="team-detail">
									<div class="detail-social">
										<ul class="social-icons nav-default inline clearfix">
											<li><a href="<?php echo $team->social_facebook?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
											<li><a href="<?php echo $team->social_twitter?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
											<li><a href="<?php echo $team->social_google?>" class="google"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="<?php echo $team->social_linkedin?>" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<div class="detail-name text-bold"><a href="<?php echo $team->url?>"><?php echo $team->title?></a></div>
									<div class="detail-title"><?php echo $team->teammate_position?></div>
								</div>
							</div>
						<?php endforeach;?>
					</div>
				</div>
				<!-- Section Content End -->
			</div>
		</div>
		<!-- Team End -->
	</section>
	<!-- Team Detail End -->
<?php include './includes/footer.inc'?>
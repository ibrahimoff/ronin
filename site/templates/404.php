<?php include './includes/header.inc'?>

	
	<!-- 404 -->
	<section id="page404">
		<!-- Section Content -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<div class="row">
					<div class="col-md-12 center">
						<div class="space"></div>
						<h1 class="title-bigbig">404</h1>
						<h2 class="title-big">Sorry! the page your are looking for doesn't exist.</h2>
						<h3>Go out take a run around the block or tap the button below.</h3>
						<div class="space"></div>
						<p><a href="index.html" class="btn btn-default">GO TO THE HOMEPAGE</a></p>
						<div class="space"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Section Content -->
	</section>
	<!-- 404 End -->
		
<?php include './includes/footer.inc'?>

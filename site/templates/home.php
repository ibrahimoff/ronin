<?php include './includes/header.inc';?>
<?php 
	$i = 1;
	$slides=[];
	$html=''; 
	foreach($pages->get("/clients/")->clients as $client){
			$slides[$i][$client->id]['logo'] = $client->client_logo->url;
			$slides[$i][$client->id]['name'] = $client->client_name;
			$slides[$i][$client->id]['link'] = $client->client_link;
			if(count($slides[$i]) == 6){
				$i++;
			}
	};?>
	<!-- Slider -->
	<section id="site-slider">
		<div class="wrapper">
			<ul class="bxslider">
				<!-- Slider Item -->
				
				<?php foreach($page->slider_items as $item){?>
					<li>
					<div class="slide-image"><img src="<?php echo $item->slider_image->url;?>" alt="<?php echo $item->slider_title;?>" class="img-full" /></div>
					<div class="slide-caption padding-all">
						<div class="wrapper">
							<div class="v-center">
								<h5 class="line-bottom"><?php echo $item->slider_title;?></h5>
								<h2 class="text-space"><?php echo $item->slider_heading;?></h2>
								<h3><?php echo $item->slider_subheading;?></h3>
								<div class="button-group clearfix">
									<a class="btn btn-border" href="<?php echo $item->slider_button_left_link;?>"><?php echo $item->slider_button_left_name;?></a>
									<a class="btn btn-border" href="<?php echo $item->slider_button_right_link;?>"><?php echo $item->slider_button_right_name;?></a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<?php }?>
				<!-- Slider Item End -->
			</ul>
		</div>
	</section>
	<!-- Slider End -->

	<!-- Services -->
	<section id="services" class="box-white">
		<div class="wrapper padding-all">
			<!-- Section Header -->
			<div class="section-header">
				<h2><?php echo $pages->get("/services/")->page_label?></h2>
				<h3><?php echo $pages->get("/services/")->page_subheading?></h3>
				<a href="<?php echo $pages->get("/services/")->url?>" class="btn btn-default v-center"><?php echo __('VIEW ALL SERVICES')?></a>
			</div>
			<!-- Section Header End -->

			<!-- Grid List -->
			<div class="grid-list">
				<div class="row margin-none">
					<!-- Grid Col -->
					<?php foreach($pages->get("/services/")->services as $service):?>
						<div class="grid col-md-4 col-sm-6">
							<p><i class="<?php echo $service->icon?>"></i></p>
							<h4 class="text-semibold text-space"><?php echo $service->title?></h4>
							<?php echo $service->body?>
							<p class="line-bottom"></p>
						</div>
					<?php endforeach;?>
					<!-- Grid Col End-->
				</div>
			</div>
			<!-- Grid List End -->
		</div>
	</section>
	<!-- Services End -->
	
	<!-- Works -->
	<section id="works" class="box-grey">
		<div class="wrapper padding-all">
			<!-- Section Header -->
			<div class="section-header">
				<h2><?php echo $pages->get("/works/")->title?></h2>
				<h3><?php echo $pages->get("/works/")->headline?></h3>
			</div>
			<!-- Section Header End -->
			
			<!-- Section Content -->
			<div class="works-horizontal" id="owl-works">
				<?php foreach($pages->get("/works/")->children as $work):?>
				<div class="item">
					<div class="row margin-none">
						<!-- Project Image -->
						<div class="col-md-6 col-sm-12 padding-none padding-right-40">
							<img src="<?php echo $work->media->get('selected=1')->image->url?>" alt="<?php echo $work->title?>" class="img-full">
						</div>
						<!-- Project Image End -->
						
						<!-- Project Description -->
						<div class="col-md-6 col-sm-12 padding-none">
							<h4 class="title-big"><?php echo $work->title?></h4>
							<?php echo $work->body?>
							<ul class="list-default">
								<li><b><?php echo __('Client')?>:</b> <?php echo $work->work_client?></li>
								<li><b><?php echo __('Category')?>:</b> <?php echo $work->work_category?></li>
								<li><b><?php echo __('Date')?>:</b>  <?php echo $work->work_date?></li>
							</ul>
							<div class="space"></div>
							<p><a href="<?php echo $work->url?>" class="btn btn-default"><?php echo __('VIEW DETAILS')?></a></p>
						</div>
						<!-- Project Description End -->
					</div>
				</div>
				<?php endforeach;?>
			</div>
			<!-- Section Content End -->
		</div>
	</section>
	<!-- Works End -->
	
	<!-- Customers -->
	<section id="customers" class="col-half">
		<div class="wrapper">
			<div class="row margin-none col-half">
				<!-- Clients -->
				<div class="col-md-6 col-sm-12 half box-dark padding-all center">
					<!-- Section Header -->
					<div class="section-header">
						<h2><?php echo $pages->get("/clients/")->title?></h2>
						<h3><?php echo $pages->get("/clients/")->page_subheading?></h3>
					</div>
					<!-- Section Header End -->
					
					<!-- Section Content -->
					<div class="grid-list grid-clients grid-dark"  id="owl-clients">
						<?php foreach($slides as $slide):?>
						<div class="item">
							<div class="row margin-none">
								<?php foreach($slide as $client):?>
								<div class="grid col-md-4 col-sm-4 col-xs-6">
									<a href="//<?php echo $client['link']?>" target="_blanc"><img src="<?php echo $client['logo']?>" alt="<?php echo $client['name']?>" class="img-full"></a>
								</div>
								<?php endforeach;?>
							</div>
						</div>
						<?php endforeach;?>

					
					</div>
					<!-- Section Content End -->
				</div>
				<!-- Clients End -->

				<!-- Testimonials -->
				<div class="col-md-6 col-sm-12 half box-yellow padding-all center">
					<!-- Section Header -->
					<div class="section-header">
						<h2><?php echo $pages->get("/clients/")->headline?></h2>
						<h3><?php echo $pages->get("/clients/")->after_headline_text?></h3>
					</div>
					<!-- Section Header End -->
					
					<!-- Section Content -->
					<div class="testimonials-horizontal" id="owl-testimonials">
						<?php foreach($pages->get("/clients/")->testimonials as $testimonial):?>
							<div class="item">
								<div class="item-image circle"><img src="<?php echo $testimonial->client_image->url?>" alt="<?php echo $testimonial->client_name?>" class="img-full"></div>
								<div class="item-comment"><?php echo $testimonial->body?></div>
								<div class="item-name"><b><?php echo $testimonial->client_name?></b></div>
								<div class="item-name"><?php echo $testimonial->client_position?></div>
							</div>
						<?php endforeach;?>
					</div>
					<!-- Section Content End -->
				</div>
				<!-- Testimonials End -->
			</div>
		</div>
	</section>
	<!-- Customers End -->
	
	<!-- Team -->
	<section id="team" class="box-grey">
		<div class="wrapper padding-all">
			<!-- Section Header -->
			<div class="section-header">
				<h2><?php echo $page->page_label?></h2>
				<h3><?php echo $page->page_subheading?></h3>
			</div>
			<!-- Section Header End -->
			
			<!-- Section Content -->
			<div class="row margin-none team-half">
				<div class="col-md-6 col-sm-12 padding-none padding-right-40">
					<!-- Team Detail -->
					
					<div class="team-half-detail">
						<!-- Team Member -->
					<?php foreach($page->custom_repeater as $item):?>
						<div id="team-member-<?php echo $item->id?>">
							<div class="team-header clearfix">
								<div class="team-photo pull-left"><a href="<?php echo $item->image->url?>" data-rel="prettyPhoto" class="zoom-effect"><img src="<?php echo $item->image->url?>" alt="<?php echo $item->title?>" class="img-full"></a></div>
								<div class="team-name pull-left">
									<h4><?php echo $item->headline?></h4>
									<h5><?php echo $item->after_headline_text?></h5>
								</div>
							</div>
							<div class="team-desc">
								<?php echo $item->body?>
							</div>
						</div>
						<!-- Team Member End -->
					<?php endforeach;?>

					</div>
					<!-- Team Detail End -->
				</div>
				<div class="col-md-6 col-sm-12 padding-none">
					<!-- Team List -->
					<div class="team-half-list">
						<div class="row">
							<?php foreach($page->custom_repeater as $item):?>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="team-member"><a href="#team-member-<?php echo $item->id?>" class="zoom-effect"><img src="<?php echo $item->image->url?>" alt="<?php echo $item->title?>" class="img-full"></a></div>
							</div>
							<?php endforeach;?>
						</div>
					</div>
					<!-- Team List End -->
				</div>
			</div>
			<!-- Section Content End -->
		</div>
	</section>
	<!-- Team End -->
	
	<!-- About -->
	<section id="about" class="box-white">
		<div class="wrapper padding-all">
			<!-- Section Content -->
			<div class="row margin-none">
				<div class="col-md-6 col-sm-12 padding-none padding-right-40">
					<!-- About Header -->
					<div class="section-header">
						<h2><?php echo $pages->get("/about/")->title?></h2>
						<h3><?php echo $pages->get("/about/")->page_subheading?></h3>
					</div>
					<!-- About Header End -->
					
					<!-- About Content -->
					<?php echo $pages->get("/about/")->body?>
					<div class="space"></div>
					<div class="button-group clearfix">
						<a href="<?php echo $pages->get("/about/")->url?>" class="btn btn-default"><?php echo __('VIEW DETAILS')?></a>
						<a href="<?php echo $pages->get("/contact/")->url?>" class="btn btn-dark"><?php echo __('GET IN TOUCH')?></a>
					</div>
					<!-- About Content End -->
				</div>
				<div class="col-md-6 col-sm-12 padding-none">
					<!-- Counter List -->
					<div class="grid-list counter-list">
						<div class="row margin-none">
							<!-- Grid Col -->
							<?php foreach($pages->get("/about/")->counters as $counter):?>
							<div class="grid col-md-6 col-sm-6">
								<span class="countTo text-semibold text-space" data-from="0" data-to="<?php echo $counter->counter_value?>" data-speed="5000" data-refresh-interval="50"></span>
								<p class="line-top"><?php echo $counter->counter_name?></p>
							</div>
							<?php endforeach;?>
							<!-- Grid Col End-->
						</div>
					</div>
					<!-- Counter List End -->
				</div>
			</div>
			<!-- Section Content End -->
		</div>
	</section>
	<!-- About End -->
	
	<!-- Blog -->
	<section id="blog" class="box-white border-top">
		<div class="wrapper padding-all">
			<!-- Section Header -->
			<div class="section-header">
				<h2><?php echo $pages->get('/blog/')->title?></h2>
				<h3><?php echo $pages->get('/blog/')->page_subheading?></h3>
			</div>
			<!-- Section Header End -->
			
			<!-- Section Content -->
			<div class="row">
				<div class="blog-horizontal" id="owl-blog">
					<?php foreach($pages->get('/blog/')->children('limit=9,sort=-date') as $child):?>
					<div class="post">
						<!-- Post Photo -->
						<div class="post-photo hover-effect">
							<figure>
								<img src="<?php echo $child->image->url?>" alt="<?php echo $child->title?>" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/blog-1.jpg" data-rel="prettyPhoto" title="<?php echo $child->title?>"><i class="fa fa-search"></i></a>
										<a href="<?php echo $child->url?>"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<!-- Post Photo End -->
						
						<!-- Post Description -->
						<div class="post-desc">
							<h3><a href="<?php echo $child->url?>"><?php echo $child->title?></a></h3>
						</div>
						<!-- Post Description End -->
						
						<!-- Post Info -->
						<div class="post-info clearfix">
							<div class="info-date pull-left"><i class="fa fa-calendar"></i><?php echo $child->date?></div>
						</div>
						<!-- Post Info End -->
					</div>
					<?php endforeach;?>
				</div>
			</div>
			<!-- Section Content End -->
		</div>
	</section>
	<!-- Blog End -->
	
	<!-- Contact -->
	<section id="contact" class="box-grey">
		<div class="wrapper padding-all">
			<!-- Section Header -->
			<div class="section-header">
				<h2><?php echo $pages->get('/contact/')->title?></h2>
				<h3><?php echo $pages->get('/contact/')->page_suheading?></h3>
			</div>
			<!-- Section Header End -->
			
			<!-- Section Content -->
			<div class="row margin-none">
				<div class="col-md-6 col-sm-12 padding-none padding-right-40">
					<!-- Contact Information -->
					<ul class="address-list">
							<li><i class="fa fa-map-marker"></i><?php echo $pages->get('/contact/')->contact_address?></li>
							<li><i class="fa fa-phone"></i><?php echo $pages->get('/contact/')->contact_phone?></li>
							<li><i class="fa fa-mobile-phone"></i><?php echo $pages->get('/contact/')->contact_number?></li>
							<li><i class="fa fa-send"></i><a href="mailto:<?php echo $pages->get('/contact/')->contact_email?>"><?php echo $pages->get('/contact/')->contact_email?></a></li>
					</ul>
					<!-- Contact Information End -->
					
					<!-- Space -->
					<div class="space"></div>
					<!-- Space End -->
					
					<!-- Contact Form -->
					<div class="data-form">
						<form action="post">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<label><?php echo __('Name')?></label>
								<div class="form-field">
									<i class="fa fa-user"></i>
									<input type="text" name="name" placeholder="<?php echo __('Enter your name')?>" required>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<label><?php echo __('E-Mail')?></label>
								<div class="form-field">
									<i class="fa fa-envelope-o"></i>
									<input type="email" name="name" placeholder="<?php echo __('Enter your email')?>" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<label><?php echo __('Telephone')?></label>
								<div class="form-field">
									<i class="fa fa-phone"></i>
									<input type="tel" name="name" placeholder="<?php echo __('Enter your telephone')?>" required>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<label><?php echo __('Website')?></label>
								<div class="form-field">
									<i class="fa fa-globe"></i>
									<input type="url" name="name" placeholder="<?php echo __('Enter your website')?>" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label><?php echo __('Message')?></label>
								<div class="form-field">
									<i class="fa fa-pencil-square-o"></i>
									<textarea name="name" placeholder="<?php echo __('Enter your message')?>" required></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<input type="submit" class="btn btn-default" value="<?php echo __('SEND YOUR MESSAGE')?>">
							</div>
						</div>
						</form>
					</div>
					<!-- Contact Form End -->
				</div>
				<div class="col-md-6 col-sm-12 padding-none">
					<!-- Contact Map -->
					<div class="google-map" data-latitude="<?php echo $pages->get('/contact/')->map_latitude?>"  data-longitude="<?php echo $pages->get('/contact/')->map_longitude?>"></div>
					<!-- Contact Map End -->
				</div>
			</div>
			<!-- Section Content End -->
		</div>
	</section>
	<!-- Contact End -->
<?php include './includes/footer.inc';?>

 
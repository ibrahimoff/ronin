<?php include './includes/header.inc'?>
	
	<!-- Price Table -->
	<section id="elements">
		<!-- Section Header -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->page_subheading?></h3>
				</div>
			</div>
		</div>
		<!-- Section Header End -->
		
		<!-- Section Content -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Price Table -->
				<div class="row margin-none price-table">
					<?php foreach($page->children as $child):?>
					<div class="col-md-3 col-sm-6 col-xs-12 padding-none">
						<div class="table-item">
							<div class="table-header">
								<div class="header-title"><?php echo $child->title?></div>
								<div class="header-price">
									<?php echo $child->headline?>
									<p><?php echo $child->after_headline_text?></p>
								</div>
							</div>
							<div class="table-content">
								<ul>
									<?php foreach($child->price_repeater as $repeater):?>
									<li><?php echo $repeater->price_field?></li>
									<?php endforeach;?>
								</ul>
							</div>

						</div>
					</div>
					<?php endforeach;?>
					<!-- Table Item -->


				</div>
				<!-- Price Table End -->
			</div>
		</div>
		<!-- Section Content End -->
	</section>
	<!-- Price Table End -->
<?php include './includes/footer.inc'?>
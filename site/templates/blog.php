<?php include './includes/header.inc';?>
	

	
	<!-- Blog -->
	<section id="blog">
		<!-- Section Header -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->page_subheading?></h3>
				</div>
			</div>
		</div>
		<!-- Section Header End -->
		
		<!-- Section Content -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Blog Categories -->
				<div class="row margin-none">
					<nav class="button-group mini clearfix">
						<a href="<?php echo $page->url?>" data-id="" class="btn <?php echo empty($input->get('category'))?'btn-dark':'btn-default'?>">ALL CATEGORIES</a>
						<?php foreach($pages->get("/blog_categories/")->children as $child):?>
							<a href="<?php echo $page->url.'?category='.$child->name?>" data-id="<?php echo $child->id?>" class="btn <?php echo $input->get('category')==$child->name?'btn-dark':'btn-default'?>"><?php echo $child->title?></a>
						<?php endforeach;?>
					</nav>
					<div class="space"></div>
				</div>
				<!-- Blog Categories End -->
				
				<div class="row">
					<div class="blog-horizontal blog-list masonry-list">
						<!-- Post Width Size -->
						<div class="grid-sizer col-md-3 col-sm-6 col-xs-12"></div>
						<!-- Post Width Size End -->
						
						
											<?php 
					$categoryID = !empty($input->get('category'))? "post_category=".$input->get('category'):'';
					$results = $page->children("limit=8,".$categoryID);
					$pagination = $results->renderPager(array(
					    'nextItemLabel' => false,
					    'previousItemLabel' => false,
						'currentItemClass'=>'active',
						'getVars'=>['category'=>$input->get('category')],
					    'listMarkup' => "<div class='row margin-none'><div class='col-lg-12'><ul class='nav-default nav-pager clearfix'>{out}</ul></div></div>",
					    'itemMarkup' => "<li class='{class}'>{out}</li>",
					    'linkMarkup' => "<a href='{url}'>{out}</a>",
					));
					
					foreach($results as $child):?>
						<div class="post masonry-item col-md-3 col-sm-6 col-xs-12">
							<!-- Post Photo -->
							<div class="post-photo hover-effect">
								<figure>
									<img src="<?php echo $child->image->url?>" alt="<?php echo $child->title?>" class="img-full">
									<figcaption class="transparent">
										<div class="caption-buttons clearfix">
											<a href="<?php echo $child->image->url?>" data-rel="prettyPhoto" title="<?php echo $child->title?>"><i class="fa fa-search"></i></a>
											<a href="<?php echo $child->url?>"><i class="fa fa-file-text"></i></a>
										</div>
									</figcaption>
								</figure>
							</div>
							<!-- Post Photo End -->
							
							<!-- Post Description -->
							<div class="post-desc">
								<h3><a href="<?php echo $child->url?>"><?php echo $child->title?></a></h3>
								<p><?php echo $child->textarea?></p>
							</div>
							<!-- Post Description End -->
							
							<!-- Post Info -->
							<div class="post-info clearfix">
								<div class="info-date pull-left"><i class="fa fa-calendar"></i><?php echo $child->date?></div>
						
							</div>
							<!-- Post Info End -->
						</div>
					<?php endforeach;?>
					<!-- Work Col End -->
					<div class="clearfix"></div>

					</div>
					<?php echo $pagination;?>
				</div>
				
			
			</div>
		</div>
		<!-- Section Content End -->
	</section>
	<!-- Blog End -->
		

		
<?php include './includes/footer.inc';?>

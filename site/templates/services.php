<?php include './includes/header.inc'?>
	
	<!-- Services -->
	<section id="services">
		<!-- Service Top -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1></h1>
					<h3><?php echo $page->page_subheading?></h3>
				</div>
				<!-- Section Header End -->
				
				<div class="space"></div>
				
				<!-- Section Content -->
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="bgr_imac">
						<img src="<?php echo $page->image->url?>" alt="<?php echo $page->title?>" class="img-full">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<h4 class="title-big"><?php echo $page->headline?></h4>
						<?php echo $page->body?>
						<div class="space"></div>
						<div class="button-group clearfix">
							<?php foreach($page->buttons as $button):?>
								<a href="<?php echo $button->button_link?>" class="btn <?php echo $button->button_class == '1'?'btn-default':'btn-dark'?>"><?php echo $button->button_name?></a>
							<?php endforeach;?>
						</div>
					</div>
				</div>
				<!-- Section Content End -->
			</div>
		</div>
		<!-- Service Top End -->
		
		<!-- Service List -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="grid-list">
					<div class="row margin-none">
						<?php foreach ($page->services as $service):?>
						<!-- Grid Col -->
						<div class="grid col-md-4 col-sm-6">
							<p><i class="<?php echo $service->icon?>"></i></p>
							<h4 class="text-semibold text-space"><?php echo $service->title?></h4>
							<?php echo $service->body?>
							<p class="line-bottom"></p>
						</div>
						<!-- Grid Col End-->
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
		<!-- Service List End -->
		
		<!-- Section Banner -->
		<div class="box-dark-light">
			<div class="wrapper padding-all">
				<div class="section-header banner">
					<h3><?php echo $page->line_text?></h3>
					<a href="<?php echo $page->button_link?>" class="btn btn-default v-center"><?php echo $page->button_name?></a>
				</div>
			</div>
		</div>
		<!-- Section Banner End -->
	</section>
	<!-- Services End -->
		
<?php include './includes/footer.inc'?>
<?php include './includes/header.inc'?>
	<!-- About -->
	<section id="about">
		<!-- About -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<!-- Section Content -->
				<div class="row margin-none">
					<div class="col-md-6 col-sm-12 padding-none padding-right-40">
						<!-- About Content -->
						<div class="section-header inline">
							<h1><?php echo $page->title?></h1>
							<h3><?php echo $page->page_subheading?></h3>
						</div>
						
						<div class="space"></div>
						<?php echo $page->body?>
						<div class="space"></div>
						<div class="button-group clearfix">
							<?php foreach($page->buttons as $button):?>
								<a href="<?php echo $button->button_link?>" class="btn <?php echo $button->button_class == '1' ?'btn-default': 'btn-dark'?>"><?php echo $button->button_name?></a>
							<?php endforeach;?>
						</div>
						<!-- About Content End -->
					</div>
					<div class="col-md-6 col-sm-12 padding-none">
						<div class="bgr_imac">
						<img src="<?php echo $page->image->url?>" alt="<?php echo $page->title?>" class="img-full">
						</div>
					</div>
				</div>
				<!-- Section Content -->
			</div>
		</div>
		<!-- About End -->
		
		<!-- Counter -->
		<div class="box-dark parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $config->urls->templates?>/img/slide-1.jpg);">
			<div class="shadow-bg">
				<div class="wrapper padding-all">
					<div class="grid-list grid-none counter-list">
						<div class="row margin-none">
							<!-- Grid Col -->
							<?php foreach ($page->counters as $counter):?>
								<div class="grid col-md-3 col-sm-6">
									<span class="countTo text-semibold text-space" data-from="0" data-to="<?php echo $counter->counter_value?>" data-speed="5000" data-refresh-interval="50"></span>
								<p class="line-top"><?php echo $counter->counter_name?></p>
							</div>
							<?php endforeach;?>
							<!-- Grid Col End-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Counter End -->
		
		<!-- Team -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header">
					<h2><?php echo $page->headline?></h2>
					<h3><?php echo $page->after_headline_text?></h3>
				</div>
				<!-- Section Header End -->
				<!-- Section Content -->
				<div class="row">
					<div class="team-horizontal" id="owl-team">			
						<?php foreach($page->children as $team):?>
							<div class="team">
								<div class="team-pic"><a href="<?php echo $team->url?>"><img src="<?php echo  $team->teammate_photo->url?>" alt="<?php echo $team->title?>" class="img-full"></a></div>
								<div class="team-detail">
									<div class="detail-social">
										<ul class="social-icons nav-default inline clearfix">
											<li><a href="<?php echo $team->social_facebook?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
											<li><a href="<?php echo $team->social_twitter?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
											<li><a href="<?php echo $team->social_google?>" class="google"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="<?php echo $team->social_linkedin?>" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<div class="detail-name text-bold"><a href="<?php echo $team->url?>"><?php echo $team->title?></a></div>
									<div class="detail-title"><?php echo $team->teammate_position?></div>
								</div>
							</div>
						<?php endforeach;?>
					</div>
				</div>
				<!-- Section Content End -->
			</div>
		</div>
		<!-- Team End -->

	<!-- About End -->
<?php include './includes/footer.inc'?>
<?php if(!$config->ajax) include './includes/header.inc'?>

	<!-- Works -->
	<section id="works">
		<!-- Selected Works -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header inline">
					<h1><?php echo $page->page_subheading?></h1>
					<h3><?php echo $page->page_label?></h3>
				</div>
				<!-- Section Header End -->
				
				<div class="space"></div>

				<!-- Section Content -->
				<div class="works-horizontal" id="owl-works">
					<?php foreach($page->find("parent=/works/, work_selected=1") as $child):?>
						<div class="item">
							<div class="row margin-none">
								<!-- Project Image -->
							
								<div class="col-md-6 col-sm-12 padding-none padding-right-40 ">
										<div class="bgr_imac">
									<img src="<?php echo $child->media->get('selected=1')->image->url?>" alt="<?php echo $child->media->work_title?>" class="img-full">
								</div>
								</div>

								<!-- Project Image End -->
								
								<!-- Project Description -->
								<div class="col-md-6 col-sm-12 padding-none">
									<h4 class="title-big"><?php echo $child->title?></h4>
									<?php echo $child->body?>
									<ul class="list-default">
										<li><b><?php echo __('Client')?>:</b> <?php echo $child->work_client?></li>
										<li><b><?php echo __('Category')?>:</b> <?php echo $child->work_category->title?></li>
										<li><b><?php echo __('Date')?>:</b> <?php echo $child->work_date?></li>
									</ul>
									<div class="space"></div>
									<p><a href="<?php echo $child->url?>" class="btn btn-default"><?php echo __('VIEW DETAILS')?></a></p>
								</div>
								<!-- Project Description End -->
							</div>
						</div>
					<?php endforeach;?>

				</div>
				<!-- Section Content End -->
			</div>
		</div>
		<!-- Selected Works End -->
		
		<!-- Our Works -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header">
					<h1><?php echo $page->headline?></h1>
					<h3><?php echo $page->after_headline_text?></h3>
				</div>
				<!-- Section Header End -->
				
				<!-- Section Content -->
				<div class="row margin-none">
					<!-- Work Categories -->
					<nav class="button-group mini clearfix">
						<a href="<?php echo $page->url?>" data-id="" class="btn <?php echo empty($input->get('category'))?'btn-dark':'btn-default'?>">ALL WORKS</a>
						<?php foreach($pages->get("/categories/")->children as $child):?>
							<a href="<?php echo $page->url.'?category='.$child->name?>" data-id="<?php echo $child->id?>" class="btn <?php echo $input->get('category')==$child->name?'btn-dark':'btn-default'?>"><?php echo $child->title?></a>
						<?php endforeach;?>
					</nav>
					<!-- Work Categories End -->
					<div class="space"></div>
				</div>
				
				<div class="row" id="works_row">
					<!-- Work Col -->
					<?php 
					$categoryID = !empty($input->get('category'))? "work_category=".$input->get('category'):'';
					$results = $page->children("limit=9,".$categoryID);
					$pagination = $results->renderPager(array(
					    'nextItemLabel' => false,
					    'previousItemLabel' => false,
						'currentItemClass'=>'active',
						'getVars'=>['category'=>$input->get('category')],
					    'listMarkup' => "<div class='row margin-none'><div class='col-lg-12'><ul class='nav-default nav-pager clearfix'>{out}</ul></div></div>",
					    'itemMarkup' => "<li class='{class}'>{out}</li>",
					    'linkMarkup' => "<a href='{url}'>{out}</a>",
					));
					
					foreach($results as $child):?>
					<div class="col-md-4 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $child->media->get('selected=1')->image->url?>" alt="<?php echo $child->title?>" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $child->media->get('selected=1')->image->url?>" data-rel="prettyPhoto" title="<?php echo $child->title?>"><i class="fa fa-search"></i></a>
										<a href="<?php echo $child->url?>"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold"><?php echo $child->title?></h4>
						<h5><?php echo $child->work_category->title?></h5>
					</div>
					<?php endforeach;?>
					<!-- Work Col End -->
					<div class="clearfix"></div>
					<?php echo $pagination;?>
				</div>
				
			</div>
		</div>
		<!-- Our Works End -->
		
		<!-- Section Banner -->
		<div class="box-dark-light">
			<div class="wrapper padding-all">
				<div class="section-header banner">
					<h3><?php echo $page->line_text?></h3>
					<a href="<?php echo $page->button_link?>" class="btn btn-default v-center"><?php echo $page->button_name?></a>
				</div>
			</div>
		</div>
		<!-- Section Banner End -->
	</section>

	<!-- Works End -->
<?php if(!$config->ajax) include './includes/footer.inc';?>

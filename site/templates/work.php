<?php include './includes/header.inc'?>
	<!-- Works Detail -->
	<section id="works">
		<!-- Section Header -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->page_subheading?></h3>
					<!--<a href="#" class="btn btn-default v-center">VIEW PROJECT</a>-->
				</div>
			</div>
		</div>
		<!-- Section Header End -->
	
		<!-- Section Content -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<div class="row">
					<!-- Project Image -->
					<div class="col-md-6 col-sm-12">
						<div id="owl-gallery">
							<?php foreach($page->media as $media):?>
							<div class="item bgr_imac">
								<?php if($media->link):?>
									<a href="<?php echo $media->link?>" data-rel="prettyPhoto" title="<?php echo $page->title?>">
										<div class="play_btn_block">
											<i class="fa fa-youtube-play"></i>
											</div>
										<img src="<?php echo $media->image->url?>" alt="<?php echo $page->title?>" class="img-full">
									</a>
								<?php else:?>
									<a href="<?php echo $media->image->url?>" data-rel="prettyPhoto" title="<?php echo $page->title?>">
										<img src="<?php echo $media->image->url?>" alt="<?php echo $page->title?>" class="img-full">
									</a>
								<?php endif;?>

							</div>
							<?php endforeach;?>

						</div>
					</div>
					<!-- Project Image End -->


					<!-- Project Description -->
					<div class="col-md-6 col-sm-12">
						<h4 class="text-bold"><?php echo __('Project Description')?></h4>
                        <?php echo $page->body?>
						<div class="space-20"></div>
						
						<h4 class="text-bold"><?php echo __('Project Details')?></h4>
						<ul class="list-default">
							<li><b><?php echo __('Client')?>:</b> <?php echo $page->work_client?></li>
							<li><b><?php echo __('Category')?>:</b> <?php echo $page->work_category->title?></li>
							<li><b><?php echo __('Date')?>:</b> <?php echo $page->work_date?></li>
						</ul>
						<div class="space-20"></div>
						
						<h4 class="text-bold margin-bottom-20">Share Project</h4>
						<ul class="social-icons nav-default clearfix">
							<li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
					<!-- Project Description End -->
				</div>
			</div>
		</div>
		<!-- Section Content End -->
		
		<!-- Our Works -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<!-- Section Header -->
				<div class="section-header">
					<h1>OTHER WORKS</h1>
					<h3>Phasellus fermentum nulla quis est blandit sollicitudin.</h3>
				</div>
				<!-- Section Header End -->
				
				<!-- Section Content -->
				<div class="row margin-none">
					<!-- Work Categories -->
					<nav class="button-group mini clearfix">
						<a href="#" class="btn btn-dark">ALL WORKS</a>
						<a href="#" class="btn btn-default">ADVERTISING</a>
						<a href="#" class="btn btn-default">DIGITAL MARKETING</a>
						<a href="#" class="btn btn-default">WEB DESIGN</a>
						<a href="#" class="btn btn-default">WEB APPLICATION</a>
						<a href="#" class="btn btn-default">SOCIAL MEDIA</a>
						<a href="#" class="btn btn-default">STRATEGY</a>
					</nav>
					<!-- Work Categories End -->
					<div class="space"></div>
				</div>
				
				<div class="row">
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-1.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-1.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Demint Winter Campaign</h4>
						<h5>Advertising</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-2.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-2.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Explore &amp; More</h4>
						<h5>Digital Marketing</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-3.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-3.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Villevil Brand Promo</h4>
						<h5>Web Design</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-4.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-4.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Asviate E-Commerce Portal</h4>
						<h5>Web Application</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-5.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-5.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Hufurza Social Media Campaign</h4>
						<h5>Social Media</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-6.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-6.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Nifralz Advertising Strategy</h4>
						<h5>Strategy</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-1.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-1.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Demint Winter Campaign</h4>
						<h5>Advertising</h5>
					</div>
					<!-- Work Col End -->
					<!-- Work Col -->
					<div class="col-md-3 col-sm-6 margin-bottom-20">
						<div class="hover-effect margin-bottom-10">
							<figure>
								<img src="<?php echo $config->urls->templates?>img/work-2.jpg" alt="Work Title" class="img-full">
								<figcaption class="transparent">
									<div class="caption-buttons clearfix">
										<a href="<?php echo $config->urls->templates?>img/work-2.jpg" data-rel="prettyPhoto" title="Work Title"><i class="fa fa-search"></i></a>
										<a href="works-detail-1.html"><i class="fa fa-file-text"></i></a>
									</div>
								</figcaption>
							</figure>
						</div>
						<h4 class="text-bold">Explore &amp; More</h4>
						<h5>Digital Marketing</h5>
					</div>
					<!-- Work Col End -->
				</div>
				
				<div class="row margin-none">
					<!-- Pager -->
					<ul class="nav-default nav-pager clearfix">
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
					</ul>
					<!-- Pager End -->
				</div>
				<!-- Section Content End -->
			</div>
		</div>
		<!-- Our Works End -->
		
		<!-- Section Banner -->
		<div class="box-dark-light">
			<div class="wrapper padding-all">
				<div class="section-header banner">
					<h3>Cras ac rutrum sem, nec congue sem venenatis eu aliquam</h3>
					<a href="#" class="btn btn-default v-center">VIEW SERVICE PLANS</a>
				</div>
			</div>
		</div>
		<!-- Section Banner End -->
	</section>
	<!-- Works Detail End -->
<?php include './includes/footer.inc'?>

	<?php include './includes/header.inc'?>

	<!-- Clients -->
	<section id="clients">
		<!-- Section Header -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->page_subheading?></h3>
				</div>
			</div>
		</div>
		<!-- Section Header End -->
		
		<!-- Client List -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="grid-list grid-clients">
					<div class="row margin-none">
						<?php foreach ($page->clients as $client):?>
							<div class="grid col-md-3 col-sm-4">
								<a href="//<?php echo $client->client_link?>" target="_blanc"><img src="<?php echo $client->client_logo->url?>" alt="<?php echo $client->client_alt?>" class="img-full"></a>
							</div>
						<?php endforeach;?>

					</div>
				</div>
			</div>
		</div>
		<!-- Client List End -->
		
		<!-- Testimonials -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<h4 class="title-big center"><?php echo $page->headline?></h4>
				<p class="line-bottom center"><?php echo $page->after_headline_text?></p>
				<div class="testimonials-horizontal" id="owl-testimonials">
					<?php foreach($page->testimonials as $testimonial):?>
					<div class="item center">
						<div class="item-image circle"><img src="<?php echo $testimonial->client_image->url?>" alt="<?php echo $testimonial->client_name?>" class="img-full"></div>
						<div class="item-comment"><?php echo $testimonial->body?></div>
						<div class="item-name"><b><?php echo $testimonial->client_name?></b></div>
						<div class="item-name"><?php echo $testimonial->client_position?></div>

					</div>
					<?php endforeach;?>
				</div>
			</div>
		</div>
		<!-- Testimonials End -->
	</section>
	<!-- Clients End -->
	<?php include './includes/footer.inc'?>
<?php 

/**
 * _main.php
 * Main markup file (multi-language)

 * MULTI-LANGUAGE NOTE: Please see the README.txt file
 *
 * This file contains all the main markup for the site and outputs the regions 
 * defined in the initialization (_init.php) file. These regions include: 
 * 
 *   $title: The page title/headline 
 *   $content: The markup that appears in the main content/body copy column
 *   $sidebar: The markup that appears in the sidebar column
 * 
 * Of course, you can add as many regions as you like, or choose not to use
 * them at all! This _init.php > [template].php > _main.php scheme is just
 * the methodology we chose to use in this particular site profile, and as you
 * dig deeper, you'll find many others ways to do the same thing. 
 * 
 * This file is automatically appended to all template files as a result of 
 * $config->appendTemplateFile = '_main.php'; in /site/config.php. 
 *
 * In any given template file, if you do not want this main markup file 
 * included, go in your admin to Setup > Templates > [some-template] > and 
 * click on the "Files" tab. Check the box to "Disable automatic append of
 * file _main.php". You would do this if you wanted to echo markup directly 
 * from your template file or if you were using a template file for some other
 * kind of output like an RSS feed or sitemap.xml, for example. 
 *
 * 
 */
?><!DOCTYPE html>
<html lang="<?php echo _x('en', 'HTML language code'); ?>">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="fb:app_id" content="945405458914193"/>
	<meta property="og:url"           content="<?php echo $page->httpUrl?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="<?php echo $page->title?>" />
	<meta property="og:description"   content="<?php echo $page->body?>" />
	<meta property="og:image"         content="<?php echo $page->image->httpUrl?>" />
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<link href="//fonts.googleapis.com/css?family=Lusitana:400,700|Quattrocento:400,700" rel="stylesheet" type="text/css" />
	<!-- Favicons -->
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/style.css" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates?>img/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo $config->urls->templates?>img/favicon.png">
	
	<!-- Css -->
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/prettyPhoto.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/fotorama.css">

	<?php
	
	// handle output of 'hreflang' link tags for multi-language
	// this is good to do for SEO in helping search engines understand
	// what languages your site is presented in	
	foreach($languages as $language) {
		// if this page is not viewable in the language, skip it
		if(!$page->viewable($language)) continue;
		// get the http URL for this page in the given language
		$url = $page->localHttpUrl($language); 
		// hreflang code for language uses language name from homepage
		$hreflang = $homepage->getLanguageValue($language, 'name'); 
		// output the <link> tag: note that this assumes your language names are the same as required by hreflang. 
		echo "\n\t<link rel='alternate' hreflang='$hreflang' href='$url' />";
	}
	
	?>
	
</head>
<body class="<?php if($sidebar) echo "has-sidebar"; ?>">
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '945405458914193',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
	<!--<div class="site-loader"><img src="<?php echo $config->urls->templates?>/img/loader.gif" alt="Loading"></div>-->
	<!-- Site Loader End -->
	
	<!-- Site Back Top -->
	<div class="site-back-top" title="Back to top">
		<i class="fa fa-angle-up"></i>
	</div>
	<!-- Site Back Top End -->
	
	<!-- Site Navigation -->
	<div class="site-nav"></div>
	<!-- language switcher / navigation -->
	
	<div id="site-container" class="site-fullscreen site-sticky">
		<header id="site-header" class="fixed">
		<div class="header-inner">
			<div class="wrapper clearfix">
				<!-- Header Logo -->
				<div class="header-logo"><a href="/">
				<img src="<?php echo $homepage->image->url?>" alt=""></a>
				</div>
				<!-- Header Logo End -->	
				<!-- Header Menu -->
				<nav class="header-menu">
						<!-- top navigation -->
					<ul class='nav-default clearfix'><?php 
						// top navigation consists of homepage and its visible children
						foreach($homepage->and($homepage->children) as $item) {
							if($item->id == $page->rootParent->id) {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							echo "<a href='$item->url'>$item->title</a></li>";
						}
				
						// output an "Edit" link if this page happens to be editable by the current user
						if($page->editable()) echo "<li class='edit'><a href='$page->editUrl'>" . __('Edit') . "</a></li>";
					?>
						<li>
						<a href="#"><?php echo $user->language->title?></a>
							<ul>
								<?php
									foreach($languages as $language) {
										if(!$page->viewable($language)) continue; // is page viewable in this language?
										if($language->id == $user->language->id) {
											echo '<li class="hidden">';
										} else {
											echo '<li>';
										}
										$url = $page->localUrl($language); 
										$hreflang = $homepage->getLanguageValue($language, 'name'); 
										echo "<a hreflang='$hreflang' href='$url'>$language->title</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- Header Menu End -->
				
				<!-- Header Nav -->
				<div class="header-nav"><i class="fa fa-bars fa-2x"></i></div>
				<!-- Header Nav End -->
			</div>
		</div>
	</header>
	<!-- Header End -->
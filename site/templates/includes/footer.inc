		<!-- Footer -->
	<footer id="site-footer">
		<div class="wrapper">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="row margin-none">
					<!-- Footer Top About -->
					<div class="col-md-6 col-sm-12 padding-none clearfix">
						<img src="<?php echo $homepage->image->url?>" alt="" class="top-logo">
						<h3 class="text-medium"><?php echo $homepage->footer_heading?></h3>
						<?php echo $homepage->footer_text?>
					</div>
					<!-- Footer Top About End -->
					
					<!-- Footer Top Menu -->
					<div class="col-md-6 col-sm-12 padding-none clearfix">
						<nav class="top-menu clearfix">
							<ul class="nav-default pull-right clearfix">
							<?php foreach($homepage->and($homepage->children) as $item) {
								if($item->id == $page->rootParent->id) {
									echo "<li class='active'>";
								} else {
									echo "<li>";
								}
								echo "<a href='$item->url'>$item->title</a></li>";
							}?>						
							</ul>
						</nav>
						<!-- Footer Top Menu End -->
						
						<!-- Footer Top Newsletter -->
						<div class="top-newsletter pull-right">
							<form>
								<input type="text" name="newsletter" placeholder="<?php echo __('Please enter your email address')?>">
								<button type="button"><i class="fa fa-paper-plane"></i></button>
							</form>
						</div>
						<!-- Footer Top Newsletter End -->
					</div>
				</div>
			</div>
			<!-- Footer Top End -->
			
			<!-- Footer Middle -->
			<div class="footer-middle">
				<div class="row margin-none">
					<!-- Footer Middle Address -->
					<div class="col-md-8 col-sm-12 padding-none">
						<ul class="address-list nav-default text-small clearfix">
							<li><i class="fa fa-map-marker"></i><?php echo $pages->get('/contact/')->contact_address?></li>
							<li><i class="fa fa-phone"></i><?php echo $pages->get('/contact/')->contact_phone?></li>
							<li><i class="fa fa-mobile-phone"></i><?php echo $pages->get('/contact/')->contact_number?></li>
							<li><i class="fa fa-send"></i><a href="mailto:<?php echo $pages->get('/contact/')->contact_email?>"><?php echo $pages->get('/contact/')->contact_email?></a></li>
						</ul>
					</div>
					<!-- Footer Middle Address End -->
					
					<!-- Footer Middle Social -->
					<div class="col-md-4 col-sm-12 padding-none">
						<ul class="social-icons nav-default pull-right clearfix">
						<?php foreach($homepage->social_fields as $social):?>
							<li><a target="_blanc" href="<?php echo $social->url_field?>" class="<?php echo $social->icon?>"><i class="fa fa-<?php echo $social->icon?>"></i></a></li>
						<?php endforeach;?>
						</ul>
					</div>
					<!-- Footer Middle Social End -->
				</div>
			</div>
			<!-- Footer Middle End -->
			
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="row margin-none">
					<div class="col-md-12 col-sm-12 padding-none">
						<p class="text-medium text-small"><?php echo $homepage->footer_copyright?></p>
					</div>
				</div>
			</div>
			<!-- Footer Bottom End -->
		</div>
	</footer>
	</div>
	<!-- Site container End -->
	 <ul class='languages'><?php
	// 	foreach($languages as $language) {
	// 		if(!$page->viewable($language)) continue; // is page viewable in this language?
	// 		if($language->id == $user->language->id) {
	// 			echo "<li class='current'>";
	// 		} else {
	// 			echo "<li>";
	// 		}
	// 		$url = $page->localUrl($language); 
	// 		$hreflang = $homepage->getLanguageValue($language, 'name'); 
	// 		echo "<a hreflang='$hreflang' href='$url'>$language->title</a></li>";
	// 	}
	// ?></ul>

	<script src="<?php echo $config->urls->templates?>/scripts/jquery.min.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/bootstrap.min.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/owl.carousel.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.bxslider.min.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.matchHeight.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.prettyPhoto.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.countTo.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.stellar.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/jquery.fitvids.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/imagesloaded.pkgd.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/masonry.pkgd.js"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/fotorama.js"></script>
	<!-- Map Scripts -->
	<script src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
	<script src="<?php echo $config->urls->templates?>/scripts/gmap3.min.js"></script>
	
	<!-- Custom Scripts -->
	<script src="<?php echo $config->urls->templates?>/scripts/scripts.js"></script>
</body>
</html>

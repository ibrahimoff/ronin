<?php include './includes/header.inc'?>

	<?php
	$validated = false;
	$form = $input->post('Contact');
	if($form) {
		foreach($form as $attr){
			if(!empty($attr)){
				$validated = true;
			}else{
				$validated = false;
			}
		}
		var_dump($form);		
		var_dump($validated);
		if($validated == true){
			if(mail('fuad.ibrahimov@live.com', "Contact Form", $form['message'], $form['email'])){
				echo 'sent';
			}else{
				echo 'error';
			}
		}
	}?>
	<!-- Contact -->
	<section id="contact">
		<!-- Section Header -->
		<div class="box-white">
			<div class="wrapper padding-all">
				<div class="section-header inline">
					<h1><?php echo $page->title?></h1>
					<h3><?php echo $page->page_subheading?></h3>
				</div>
			</div>
		</div>
		<!-- Section Header End -->
		
		<!-- Section Content -->
		<div class="box-grey">
			<div class="wrapper padding-all">
				<div class="row">
					<!-- Contact Info -->
					<div class="col-md-6 col-sm-12">
						<!-- Contact Information -->

						<ul class="address-list">
							<li><i class="fa fa-map-marker"></i><?php echo $page->contact_address?></li>
							<li><i class="fa fa-phone"></i><?php echo $page->contact_phone?></li>
							<li><i class="fa fa-mobile-phone"></i><?php echo $page->contact_number?></li>
							<li><i class="fa fa-send"></i><a href="mailto:<?php echo $page->contact_email?>"><?php echo $page->contact_email?></a></li>
						</ul>
						<ul class="social-icons nav-default clearfix">
							<?php foreach($page->social_fields as $social):?>
							<li><a href="<?php echo $social->url_field?>" class="<?php echo $social->icon?>"><i class="fa fa-<?php echo $social->icon?>"></i></a></li>
							<?php endforeach;?>
						</ul>
						<hr>
						<!-- Contact Information End -->
						
						<!-- Contact Form -->
						<div class="data-form">
							<form action="<?php echo $page->url?>" method="post">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<label><?php echo __('Name')?></label>
									<div class="form-field">
										<i class="fa fa-user"></i>
										<input type="text" name="Contact[name]" placeholder="<?php echo __('Enter your name')?>" >
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<label><?php echo __('E-Mail')?></label>
									<div class="form-field">
										<i class="fa fa-envelope-o"></i>
										<input type="email" name="Contact[email]" placeholder="<?php echo __('Enter your email')?>" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<label><?php echo __('Telephone')?></label>
									<div class="form-field">
										<i class="fa fa-phone"></i>
										<input type="tel" name="Contact[tel]" placeholder="<?php echo __('Enter your telephone')?>" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<label><?php echo __('Message')?></label>
									<div class="form-field">
										<i class="fa fa-pencil-square-o"></i>
										<textarea name="Contact[message]" placeholder="<?php echo __('Enter your message')?>"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<input type="submit" class="btn btn-default" value="<?php echo __('SEND YOUR MESSAGE')?>">
								</div>
							</div>
							</form>
						</div>
						<!-- Contact Form End -->
					</div>
					<!-- Contact Info End -->
					
					<!-- Contact Map -->
					<div class="col-md-6 col-sm-12">
						<div class="google-map" data-latitude="<?php echo $page->map_latitude?>"  data-longitude="<?php echo $page->map_longitude?>"></div>
					</div>
					<!-- Contact Map End -->
				</div>
			</div>
		</div>
		<!-- Section Content End -->
	</section>
	<!-- Contact End -->
<?php include './includes/footer.inc'?>
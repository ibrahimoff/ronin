<?php include './includes/header.inc';?>
	

	<!-- Blog Single -->
	<section id="blog" class="post-detail">
		<div class="box-dark parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $page->image->url?>);">
			<div class="shadow-bg">
				<div class="wrapper padding-all">
					<!-- Post Media -->
					<div class="post-media type-parallax">
						<div class="section-header inline clearfix">
							<h1><?php echo $page->title?></h1>
							<h3 class="text-small">By <a href="#" class="text-yellow">Jane Smith</a> In <a href="#" class="text-yellow"><?php echo $page->post_category->title?></a><?php echo $page->date?></h3>

							<ul class="social-icons nav-default margin-top-20 clearfix">
								<div class="fb-like" data-href="" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
							</ul>
						</div>
					</div>
					<!-- Post Media End -->
				</div>
			</div>
		</div>
		
		<div class="box-white">
			<div class="wrapper padding-all">
				<!-- Post Content -->
				<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="post-desc padding-80">
	                <?php echo $page->body?>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="fotorama" data-nav="thumbs" data-hash="true" data-allowfullscreen="true" data-width="100%">
					<?php foreach ($page->post_media as $child):?>
						<?=$child->link?>
						<?php if(empty($child->link)):?>
							<img src="<?php echo $child->image->url?>">
						<?php else:?>
							<a href="<?php echo $child->link?>"></a>
						<?php endif;?>
					<?php endforeach;?>	

					</div>
				</div>
				<div class="clearfix"></div>
				</div>

				<!-- Post Content End -->
			</div>
		</div>
	</section>
	<!-- Blog Single End -->
<?php include './includes/footer.inc';?>
